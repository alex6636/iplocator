<?php
namespace alexs\iplocator\tests;
use alexs\iplocator\Locator;
use PHPUnit\Framework\TestCase;
use RuntimeException;

class LocatorTest extends TestCase
{
    public function testSendRequest() {
        $ip = '5.57.95.255';
        $Locator = new Locator($ip);
        $response = $Locator->sendRequest();
        $this->assertArrayHasKey('lat', $response);
        $this->assertArrayHasKey('lon', $response);
        $this->assertArrayHasKey('region', $response);
        $this->assertArrayHasKey('country', $response);
        $this->assertArrayHasKey('countryCode', $response);
        $this->assertArrayHasKey('city', $response);
    }

    /**
     * @expectedException RuntimeException
     */
    public function testSendRequestInvalidIp() {
        $ip = 'invalid';
        $Locator = new Locator($ip);
        $Locator->sendRequest();
    }
}