<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   2017
 */

namespace alexs\iplocator;
use RuntimeException;

class Locator
{
    public $ip;
    public $request_timeout = 5;

    public function __construct($ip) {
        $this->ip = $ip;
    }

    /**
     * @return string
     */
    public function getRequestUrl() {
        return 'http://ip-api.com/php/' . $this->ip;
    }

    /**
     * @throws RuntimeException
     * @return array
     */
    public function sendRequest() {
        $url = $this->getRequestURL();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->request_timeout);
        $contents = curl_exec($ch);
        curl_close($ch);
        if ($contents === false) {
            throw new RuntimeException('Could not send request to ' . $url);
        }
        $result = @unserialize($contents);
        if ($result['status'] == 'success') {
            return $result;
        }
        throw new RuntimeException($result['message']);
    }
}
